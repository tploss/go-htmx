package main

import (
	"html/template"
	"log"
	"net/http"
)

type PageData struct {
	Title   string
	Heading string
}

func main() {
	http.HandleFunc("/", defaultHandler)
	http.HandleFunc("/films", filmsHandler)

	// Create a custom Logger
	logger := log.New(log.Writer(), "[Server] ", log.LstdFlags)

	// Wrap the default ServeMux with a custom handler that logs the requests
	http.ListenAndServe(":8080", loggingHandler(logger))
}

func defaultHandler(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/films", http.StatusFound)
}

func filmsHandler(w http.ResponseWriter, r *http.Request) {
	data := PageData{
		Title:   "Welcome to My Website",
		Heading: "My Films",
	}

	tmpl, err := template.ParseFiles("templates/films.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = tmpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// Logging middleware that wraps the default ServeMux
func loggingHandler(logger *log.Logger) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger.Printf("Request: %s %s\n", r.Method, r.URL.Path)
		http.DefaultServeMux.ServeHTTP(w, r)
	})
}
